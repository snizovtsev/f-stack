#!/bin/bash -xe

TOP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
$TOP/build-dev.sh
sudo gdb -x $TOP/gdb/common.gdb --args example/fstack-client --conf config-dev.ini --proc-type=primary --proc-id=0
