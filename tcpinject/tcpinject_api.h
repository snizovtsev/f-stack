#ifndef _FSTACK_TCPINJECT_API_H
#define _FSTACK_TCPINJECT_API_H

#include <sys/uio.h>
#include <time.h>

struct tcpinject_session;

struct tcpinject_report {
    struct timespec tx;
};

int tcpinject_create_session(int sfd, struct tcpinject_session**);
int tcpinject_write(struct tcpinject_session* is, const void* buf, size_t nbytes);
int tcpinject_steady(struct tcpinject_session* is);
int tcpinject_cancel(struct tcpinject_session* is);
int tcpinject_tx(struct tcpinject_session* is);
int tcpinject_finish(struct tcpinject_session* is, struct tcpinject_report* report);

#endif // _FSTACK_TCPINJECT_API_H
