#ifndef _FSTACK_TCPINJECT_H
#define _FSTACK_TCPINJECT_H

#include <sys/cdefs.h>
#include <sys/param.h>

enum {
    TCP_INJECT_CREATE_SESSION = 0x80000000u /*TCP_VENDOR*/ - 0x7fda,
};

struct tcpinject_session;
struct tcpinject_cb;

int tcpinject_init(void);

size_t tcpinject_cb_size(void);
int tcpinject_cb_ctor(void* mem, int size, void* arg, int flags);
void tcpinject_cb_dtor(void* mem, int size, void* arg);
int tcpinject_cb_output(struct tcpinject_cb* ticb);

int tcpinject_create_session_impl(struct tcpinject_cb* ticb, struct tcpinject_session** is);

#endif /* _FSTACK_TCPINJECT_H */
