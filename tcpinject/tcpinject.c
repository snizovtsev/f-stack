#include "tcpinject.h"

#include <stdint.h>
#include <stdlib.h>

#include <rte_cycles.h>
#include <rte_ethdev.h>
#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_mbuf.h>
#include <rte_tcp.h>

#include "ff_api.h"
#include "ff_errno.h"

enum {
    MAX_PKTS = 16,
};

static struct rte_mempool* pktmbuf_pool;

/* Extends socket state via osd(9) and khelp(9) */
struct tcpinject_cb {
    struct tcpinject_session* s;
};

struct tcpinject_session {
    struct rte_eth_dev_tx_buffer* pkts;
    uint64_t create_tsc;
    uint64_t ready_tsc;
    uint64_t steady_tsc;
};

int tcpinject_init(void)
{
    pktmbuf_pool = rte_mempool_lookup("mbuf_pool_0");
    return 0;
}

size_t tcpinject_cb_size(void)
{
    return sizeof(struct tcpinject_cb);
}

int tcpinject_cb_ctor(void* mem, int size, void* arg, int flags)
{
    struct tcpinject_cb* ticb = mem;
    ticb->s = NULL;
    return 0;
}

void tcpinject_cb_dtor(void* mem, int size, void* arg)
{
    struct tcpinject_cb* ticb = mem;
    free(ticb->s);
}

int tcpinject_create_session(int sfd, struct tcpinject_session** sout)
{
    socklen_t optlen = sizeof(void*);
    return ff_getsockopt_freebsd(sfd, IPPROTO_TCP,
        TCP_INJECT_CREATE_SESSION, *sout, &optlen);
}

int tcpinject_create_session_impl(struct tcpinject_cb* ticb, struct tcpinject_session** is)
{
    size_t pkt_size;
    struct rte_mbuf* pkt;

    pkt = rte_pktmbuf_alloc(pktmbuf_pool);
    if (pkt == NULL) {
        return ff_ENOBUFS;
    }

    pkt_size = ETHER_HDR_LEN + sizeof(struct ipv4_hdr) + sizeof(struct tcp_hdr);
    pkt->data_len = pkt_size;
    pkt->pkt_len = pkt_size;

    rte_eth_tx_burst(0, 0, &pkt, 1);
    return 0;
}
#if 0
{
    if (ticb->s != NULL) {
        return EBUSY;
    }
    *is = calloc(1, sizeof(struct tcpinject_session));
    ticb->s = *is;
    return 0;
}
#endif

int tcpinject_write(struct tcpinject_session* is, const void* buf, size_t nbytes)
{
    return 0;
}

int tcpinject_cb_output(struct tcpinject_cb* ticb)
{
    return EAGAIN;
}
