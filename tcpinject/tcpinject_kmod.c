#include "tcpinject.h"

#include <sys/cdefs.h>

#include <sys/capsicum.h>
#include <sys/event.h>
#include <sys/hhook.h>
#include <sys/kernel.h>
#include <sys/khelp.h>
#include <sys/module.h>
#include <sys/module_khelp.h>
#include <sys/osd.h>
#include <sys/param.h>
#include <sys/pcpu.h>
#include <sys/proc.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>

#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/tcp_var.h>

static struct helper tcpinject_helper;

static inline struct tcpinject_cb*
tcpinject_cb_get(struct tcpcb* tp)
{
    return khelp_get_osd(tp->osd, tcpinject_helper.h_id);
}

static int
tcpinject_output(struct tcpcb* tp)
{
    struct tcpinject_cb* ticb = tcpinject_cb_get(tp);
    if (tcpinject_cb_output(ticb) != 0) {
        return tcp_output(tp);
    } else {
        return 0;
    }
}

static int
tcpinject_usrreq_create_session(struct socket* so, struct inpcb* inp, struct tcpcb* tp,
    struct tcpinject_session** is)
{
    return tcpinject_create_session_impl(tcpinject_cb_get(tp), is);
}

static int
tcpinject_ctloutput(struct socket* so, struct sockopt* sopt,
    struct inpcb* inp, struct tcpcb* tp)
{

    switch (sopt->sopt_name) {
    case TCP_INJECT_CREATE_SESSION:
        if (sopt->sopt_dir != SOPT_GET) {
            return EINVAL;
        }
        if (sopt->sopt_val == NULL || sopt->sopt_valsize != sizeof(void*)) {
            return EINVAL;
        }

        return tcpinject_usrreq_create_session(so, inp, tp,
            (struct tcpinject_session**)sopt->sopt_val);

    default:
        return tcp_default_ctloutput(so, sopt, inp, tp);
    }
}

static struct tcp_function_block tcpfb_tcpinject = {
    .tfb_tcp_block_name = "tcpinject",
    .tfb_tcp_output = tcpinject_output,
    .tfb_tcp_do_segment = tcp_do_segment,
    .tfb_tcp_ctloutput = tcpinject_ctloutput
};

static struct hookinfo tcpinject_hooks[] = {};
static struct helper tcpinject_helper = {
    .mod_init = tcpinject_init,
    .mod_destroy = NULL,
    .h_flags = HELPER_NEEDS_OSD,
    .h_classes = HELPER_CLASS_TCP
};

#define KHELP_DECLARE_UMA(hname, hdata, hhooks, version, size, ctor, dtor) \
    static struct khelp_modevent_data kmd_##hname = {                      \
        .name = #hname,                                                    \
        .helper = hdata,                                                   \
        .hooks = hhooks,                                                   \
        .nhooks = sizeof(hhooks) / sizeof(hhooks[0]),                      \
        .uma_zsize = size,                                                 \
        .umactor = ctor,                                                   \
        .umadtor = dtor                                                    \
    };                                                                     \
    static moduledata_t h_##hname = {                                      \
        .name = #hname,                                                    \
        .evhand = khelp_modevent,                                          \
        .priv = &kmd_##hname                                               \
    };

KHELP_DECLARE_UMA(tcpinject, &tcpinject_helper, tcpinject_hooks,
    1, -1, tcpinject_cb_ctor, tcpinject_cb_dtor);

static void
tcpinject_boot(void* data)
{
    int error;

    if ((error = register_tcp_functions(&tcpfb_tcpinject, M_NOWAIT)) < 0)
        panic("register_tcp_function(tcpinject) failed: %d", error);

    kmd_tcpinject.uma_zsize = tcpinject_cb_size();
    if ((error = khelp_modevent(NULL, MOD_LOAD, &kmd_tcpinject)) < 0)
        panic("khelp_modevent failed: %d", error);
}
SYSINIT(testinit, SI_SUB_KLD, SI_ORDER_ANY, tcpinject_boot, NULL);
