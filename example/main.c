#include "ff_api.h"
#include "tcpinject_api.h"

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h> /* FIONBIO */
#include <time.h>
#include <unistd.h>

#include <rte_debug.h>

#define BALLAST "ABCDEFGHIJKLMNOPQRSUVWXYABCDEFGHIJKLMNOPQRSUVWXYZZABCDEFGHIJKLMNOPQRSUVWXYZ\033[1K\r";
const char* hellomsg = "%d\t%d\t%d\n" BALLAST;

struct client {
    struct sockaddr_in peer;

    enum {
        CLIENT_NEW = 0,
        CLIENT_CONNECTING,
        CLIENT_IDLE,
        CLIENT_WRITE,
    } state;

    int sfd;
    int succ, drop;

    int wstate, wend;
    char buf[1024];
};

void client_write(struct client* cl)
{
    if (cl->wstate == cl->wend) {
        cl->state = CLIENT_IDLE;
        ++cl->succ;
        return;
    }

    int n = ff_write(cl->sfd, &cl->buf[cl->wstate], cl->wend - cl->wstate);
    if (n >= 0) {
        cl->wstate += n;
        return client_write(cl);
    }

    if (errno != EWOULDBLOCK) {
        perror("ff_write");
        exit(1);
    }
}

void client_ping(struct client* cl)
{
    if (cl->state != CLIENT_IDLE) {
        ++cl->drop;
        return;
    }

    cl->state = CLIENT_WRITE;
    cl->wstate = 0;
    cl->wend = sprintf(cl->buf, hellomsg, time(0), cl->succ, cl->drop);
    fprintf(stderr, "PING: %s", cl->buf);
    client_write(cl);
}

void* client_ping_closure(void* arg)
{
    client_ping((struct client*)arg);
    return NULL;
}

struct client*
client_new(const char* host, uint16_t port)
{
    struct client* cl = (struct client*)calloc(1, sizeof(struct client));

    cl->peer.sin_family = AF_INET;
    cl->peer.sin_port = htons(port);
    if (inet_pton(AF_INET, host, &cl->peer.sin_addr) != 1) {
        fprintf(stderr, "Bad address: %s", host);
        goto error;
    }

    if ((cl->sfd = ff_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("ff_socket");
        goto error;
    }

    int on = 1;
    if (ff_ioctl(cl->sfd, FIONBIO, &on) < 0) {
        perror("ff_ioctl");
        goto error;
    }

    return cl;

error:
    free(cl);
    exit(1);
}

void client_connected(struct client* cl)
{
    fprintf(stderr, "CONNECTED\n");
    assert(cl->state == CLIENT_CONNECTING);

    cl->state = CLIENT_IDLE;
    cl->wstate = 0;
    return client_ping(cl);
}

void client_connect(struct client* cl)
{
    assert(cl->state == CLIENT_NEW);
    cl->state = CLIENT_CONNECTING;

    struct linux_sockaddr* addr = (struct linux_sockaddr*)&cl->peer;
    int alen = sizeof(struct sockaddr_in);

    if (!ff_connect(cl->sfd, addr, alen)) {
        return client_connected(cl);
    } else if (errno != EINPROGRESS) {
        perror("ff_connect");
        exit(1);
    }
}

void inject_line(int sfd)
{
    struct tcpinject_session* is;
    tcpinject_create_session(sfd, &is);
}

int kqfd;
volatile int usr1_req = 0;

int loop(void* arg)
{
    struct client* cl = (struct client*)arg;
    struct kevent ev[10];
    struct timespec ts = { .tv_sec = 0, .tv_nsec = 0 };
    int nev;

    if ((nev = ff_kevent(kqfd, NULL, 0, &ev[0], 10, &ts)) < 0) {
        perror("ff_kevent");
        exit(1);
    }

    for (int i = 0; i < nev; ++i) {
        if (ev[i].filter == EVFILT_WRITE) {
            if (cl->state == CLIENT_WRITE) {
                client_write(cl);
            } else if (cl->state == CLIENT_CONNECTING) {
                client_connected(cl);
            }
        }
    }

    for (int i = 0; i < nev; ++i) {
        if (ev[i].filter == EVFILT_TIMER) {
            client_ping(cl);
        }
    }

    if (usr1_req != 0) {
        usr1_req = 0;
        inject_line(cl->sfd);
    }

    return 0;
}

static void
signal_handler(int signum)
{
    switch (signum) {
    case SIGUSR1:
        usr1_req = 1;
        return;
    }
}

int main(int argc, char* argv[])
{
    signal(SIGUSR1, signal_handler);
    ff_init(argc, argv);

    struct client* cl;
    struct kevent ev[3];

    kqfd = ff_kqueue();
    if (kqfd < 0)
        rte_panic("ff_kqueue: %s", strerror(errno));

    cl = client_new("172.16.1.1", 8080);
    EV_SET(ev + 0, 0, EVFILT_TIMER, EV_ADD, 0, 1000 /*ms*/, 0);
    EV_SET(ev + 1, cl->sfd, EVFILT_WRITE, EV_ADD, 0, 0, cl);
    //EV_SET(ev+2, cl->sfd, EVFILT_READ, EV_ADD, 0, 0, cl);

    if (ff_kevent(kqfd, ev, 2, NULL, 0, NULL) < 0)
        rte_panic("kevent: %s", strerror(errno));

    client_connect(cl);
    ff_run(loop, cl);

    return EXIT_SUCCESS;
}
