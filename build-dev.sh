#!/bin/bash -xe

TOP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

make -C lib CC=gcc-7 KMOD_SRCS=tcpinject_kmod.c FF_DPDK=$TOP/dpdk/x86_64-native-linuxapp-gcc
make -C example CC=gcc-7 FF_DPDK=$TOP/dpdk/x86_64-native-linuxapp-gcc
