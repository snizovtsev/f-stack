#!/bin/bash -xe

TOP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

rm -rf "$TOP"/dpdk/x86_64-native-linuxapp-clang
make -C "$TOP"/lib clean
make -C "$TOP"/example clean

bear make -C dpdk install T=x86_64-native-linuxapp-clang
bear -a make -C lib CC=clang KMOD_SRCS=tcpinject_kmod.c FF_DPDK=$TOP/dpdk/x86_64-native-linuxapp-clang
bear -a make -C example CC=clang FF_DPDK=$TOP/dpdk/x86_64-native-linuxapp-clang
